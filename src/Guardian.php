<?php
namespace Xaamin\Guardian;

use Closure;
use UnexpectedValueException;
use Xaamin\Guardian\Contracts\UserPermissionableInterface;

class Guardian
{
    protected $policies = [];

    public function define($policy, Closure $callback)
    {
        $this->policies[$policy] = $callback;
    }

    public function isDefined($policy)
    {
        return isset($this->policies[$policy]);
    }

    protected function applyPolicy(array $params)
    {
        $policy = array_shift($params);

        if (!$this->isDefined($policy)) {
            return null;
        }

        $params = array_merge([$this->user()], $params);

        return call_user_func_array($this->policies[$policy], $params);
    }

    public function allows()
    {
        $allowed = $this->applyPolicy(func_get_args());

        if ($allowed === null) {
            $args = func_get_args();

            $ability = array_shift($args);

            $allowed = $this->user()->can($ability);
        }

        return $allowed;
    }

    public function denies()
    {
        return $this->allows(func_get_args());
    }

    public function forUser(UserPermissionableInterface $user)
    {
        $guardian = new Guardian();
        $guardian->setUser($user);

        foreach ($this->policies as $policy => $handler) {
            $guardian->define($policy, $handler);
        }

        return $guardian;
    }

    public function setUser(UserPermissionableInterface $user)
    {
        $this->user = $user;

        return $this;
    }

    public function user()
    {
        $user = $this->getUser();

        if (!$user) {
            throw new UnexpectedValueException("No user logged in");
        }

        return $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function __call($method, $parameters)
    {
        return call_user_func_array([$this->user(), $method], $parameters);
    }
}