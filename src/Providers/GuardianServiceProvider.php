<?php
namespace Xaamin\Guardian\Providers;

use Illuminate\Support\ServiceProvider;

class GuardianServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../resources/migrations/' => database_path('migrations')
        ], 'guardian-migrations');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->singleton('Xaamin\Guardian\Guardian', function ($app) {
           return new \Xaamin\Guardian\Guardian();
       });
    }
}
