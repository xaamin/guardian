<?php
namespace Xaamin\Guardian\Contracts;

interface UserPermissionableInterface extends HasRolesInterface, HasPermissionsInterface
{
}