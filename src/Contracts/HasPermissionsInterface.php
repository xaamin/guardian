<?php
namespace Xaamin\Guardian\Contracts;

interface HasPermissionsInterface
{
    public function getPermissions();

    public function can($permission, $guard = null);

    public function canNot($permission, $guard = null);
}