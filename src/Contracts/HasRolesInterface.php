<?php
namespace Xaamin\Guardian\Contracts;

interface HasRolesInterface
{
    public function getRoles();

    public function is($role, $guard = null);

    public function isNot($role, $guard = null);
}