<?php
namespace Xaamin\Guardian\Support;

trait WithRoles
{
    public function is($role, $group = null)
    {
        $role = (array)$role;
        $roles = $this->getRoles();
        $match = null;

        foreach ($roles as $item) {
            $found = in_array($item->role, $role, true) && ($group ? $item->group === $group : true);

            if ($found) {
                $match = $item;

                break;
            }
        }

        return $match !== null;
    }

    public function isNot($role, $group = null)
    {
        return !$this->is($role, $group);
    }
}
