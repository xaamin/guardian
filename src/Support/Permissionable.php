<?php
namespace Xaamin\Guardian\Support;

trait Permissionable
{
    use WithRoles;
    use WithPermissions;
}
