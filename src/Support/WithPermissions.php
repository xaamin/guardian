<?php
namespace Xaamin\Guardian\Support;

trait WithPermissions
{
    public function can($permission, $group = null)
    {
        $permission = (array)$permission;
        $permissions = $this->getPermissions();
        $match = null;

        foreach ($permissions as $item) {
            $found = in_array($item->permission, $permission, true) && ($group ? $item->group === $group : true);

            if ($found) {
                $match = $item;

                break;
            }
        }

        return $match !== null;
    }

    public function canNot($permission, $group = null)
    {
        return !$this->can($permission, $group);
    }
}
