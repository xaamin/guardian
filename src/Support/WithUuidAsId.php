<?php
namespace Xaamin\Guardian\Support;

use Illuminate\Support\Str;

trait WithUuidAsId
{
    /**
     * Initialize the trait.
     *
     * @return void
     */
    public function initializeWithUuidAsId()
    {
        $this->incrementing = $this->getIncrementing();
        $this->keyType = $this->getKeyType();
    }

    /**
     * Cast to proper data type for id.
     */
    protected static function bootWithUuidAsId()
    {
        static::creating(function ($model) {
            if (!$model->{$model->getKeyName()} && config('guardian.uuids')) {
                $model->{$model->getKeyName()} = (string) Str::orderedUuid();
            }
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return config('guardian.uuids') === true
            ? false
            : $this->incrementing;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return config('guardian.uuids') === true
            ? 'string'
            : $this->keyType;
    }
}
