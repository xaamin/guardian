<?php
namespace Xaamin\Guardian\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Xaamin\Guardian\Support\WithUuidAsId;

class Role extends Model
{
    use WithUuidAsId;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    public function getConnectionName()
    {
        return config('guardian.connection');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions');
    }
}
