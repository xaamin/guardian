<?php
namespace Xaamin\Guardian\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Xaamin\Guardian\Support\WithUuidAsId;

class Permission extends Model
{
    use WithUuidAsId;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    public function getConnectionName()
    {
        return config('guardian.connection');
    }
}
