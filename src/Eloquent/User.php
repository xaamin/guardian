<?php
namespace Xaamin\Guardian\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Xaamin\Guardian\Support\Permissionable;
use Xaamin\Guardian\Contracts\UserPermissionableInterface;
use Xaamin\Guardian\Support\WithUuidAsId;

class User extends Model implements UserPermissionableInterface
{
    use WithUuidAsId;
    use Permissionable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public function getConnectionName()
    {
        return config('guardian.connection');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'user_permissions');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPermissions()
    {
        return $this->permissions;
    }
}
