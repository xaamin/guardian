<?php
namespace Xaamin\Guardian\Facades;

use Illuminate\Support\Facades\Facade;

class Guardian extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Xaamin\Guardian\Guardian';
    }
}