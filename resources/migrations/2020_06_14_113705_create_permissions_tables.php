<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection = config('guardian.connection');

        Schema::connection($connection)->create('permissions', function (Blueprint $table) {
            if (config('guardian.uuids')) {
                $table->uuid('id')->primary();
            } else {
                $table->increments('id');
            }

            $table->string('group', 36)->default('*');
            $table->string('tag', 36)->default('*');
            $table->string('permission', 150);
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::connection($connection)->create('roles', function (Blueprint $table) {
            if (config('guardian.uuids')) {
                $table->uuid('id')->primary();
            } else {
                $table->increments('id');
            }

            $table->string('group', 36)->default('*');
            $table->string('tag', 36)->default('*');
            $table->string('role', 100);
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::connection($connection)->create('role_permissions', function (Blueprint $table) {
            if (config('guardian.uuids')) {
                $table->uuid('role_id');
                $table->uuid('permission_id');
            } else {
                $table->unsignedInteger('role_id');
                $table->unsignedInteger('permission_id');
            }

            $table->foreign('permission_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');

            $table->primary(['role_id', 'permission_id'], 'role_permissions_primary');
        });

        Schema::connection($connection)->create('user_permissions', function (Blueprint $table) {
            if (config('guardian.uuids')) {
                $table->uuid('user_id');
                $table->uuid('permission_id');
            } else {
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('permission_id');
            }

            $table->primary(['user_id', 'permission_id'], 'user_permissions_primary');

            $table->foreign('permission_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade');
        });

        Schema::connection($connection)->create('user_roles', function (Blueprint $table) {
            if (config('guardian.uuids')) {
                $table->uuid('user_id');
                $table->uuid('role_id');
            } else {
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('role_id');
            }

            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');

            $table->primary(['role_id', 'user_id'], 'user_roles_role_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_roles');
        Schema::drop('user_permissions');
        Schema::drop('role_permissions');
        Schema::drop('roles');
        Schema::drop('permissions');
    }
}
